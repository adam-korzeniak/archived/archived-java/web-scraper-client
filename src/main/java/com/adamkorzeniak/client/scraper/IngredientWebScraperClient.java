package com.adamkorzeniak.client.scraper;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Service
@RequiredArgsConstructor
//FIXME: Refactor
//FIXME: Tests
public class IngredientWebScraperClient {

    private final EndpointConfig endpointConfig;
    private final RestTemplate restTemplate;

    public IngredientResponse getIngredient(String name, String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Basic YWRtaW46YWRtaW4=");
        HttpEntity<Void> entity = new HttpEntity<>(headers);
        URI uri = buildURI(name, url);
        ResponseEntity<IngredientResponse> response = restTemplate.exchange(uri, HttpMethod.GET, entity, IngredientResponse.class);
        return response.getBody();
    }

    private URI buildURI(String name, String url) {
        return UriComponentsBuilder.fromUriString(endpointConfig.getWebScraper().getUrl())
                .path(endpointConfig.getWebScraper().getIleWazyPath())
                .queryParam("name", name)
                .queryParam("url", url)
                .build().toUri();
    }
}
